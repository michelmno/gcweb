<?php

/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
?>

<div id="chargement" class="box">
        <p><?php echo __('Veuillez patienter le chargement de cette page peut être long') ?>.</p>

        <p>[<a href="#" onclick="javascript:hide('chargement')"><?php echo __('masquer ce message') ?></a>]</p>
    </div>

<?php
$items = $bdd;
if ($collec['nbItems'])
    $collec['pages'] = 1;
else
    $collec['pages'] = 0;
include TEMPLATE_MODEL_PATH_GCWEB.'/list_'.$collec['type'].'.php';
?>
