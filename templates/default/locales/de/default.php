<?php
/*** fichier lang généré par po2phparray.py by Jonas (http://jonas.tuxfamily.org/wiki/po2phparray) ***/

$lang = array_merge($lang, array(
'Informations'
 => 'Informationen',
'Type de collection'
 => 'Sammlungs Typ',
'Nombre d\'éléments'
 => 'Anzahl Objekte',
'Dernière mise à jour'
 => 'Letzte Aktualisierung',
'%A %e %B %Y'
 => '%A %e %B %Y',
'Voir la collection'
 => 'Sammlung ansehen',
'Derniers éléments ajoutés'
 => 'Zuletzt hinzugefügte Objekte',
'S\'abonner (RSS 2.0)'
 => 'Abonnieren (RSS 2.0)',
'Collection inconnue pour le thème'
 => 'Sammlungstyp unbekannt für dieses Design',
'Les fichiers de thème de cette collection n\'ont pas encore été créés. <a href="%s&amp;generator=True">Cliquez ici</a> pour les créer'
 => 'Die Design-Datei für diesen Sammlungstyp wurde noch nicht erstellt. <a href="%s&amp;generator=True">Klicken Sie hier</a>, um sie zu erstellen.',
'Erreur : La page demandée n\'existe pas'
 => 'Fehler : die angeforderte Seite existiert nicht',
'Collections'
 => 'Sammlungen',
'Vous avez désactivé javascript. Certaines parties du site seront inaccessibles.'
 => 'Sie haben Javascript deaktiviert. Teile der Seite sind unzugänglich.',
'Fermer'
 => 'Schliessen',
'<span class="strong">Note :</span> Certaines informations peuvent provenir de sources externes. Pour les connaître, cliquez sur le lien "source" de l\'élément'
 => '<span class="strong">Hinweis:</span> Einige Informationen können aus externen Quellen kommen. Klicken Sie auf den "Quelle"-Link des Artikels um diese zu erreichen.',
'Propulsé par'
 => 'Powered by',
'Affichage'
 => 'Anzeige',
'Standard par pages'
 => 'Standard pro Seite',
'Le chargement peut être long'
 => 'Das laden kann etwas dauern',
'Standard tous'
 => 'Standard alle',
'Description'
 => 'Beschreibung',
'Mosaïque'
 => 'Mosaik',
'Exportation'
 => 'Export',
'CSV (classeur)'
 => 'CSV (Mappe)',
'Texte brut'
 => 'Klartext',
'Genres'
 => 'Genre',
'Langues'
 => 'Sprache',
'Sous-titres'
 => 'Untertitel',
'Séries'
 => 'Serien',
'Auteurs'
 => 'Autoren',
'Editeurs'
 => 'Herausgeber',
'Mécanismes'
 => 'Mechanismus',
'Éditeurs'
 => 'Herausgeber',
'Années de publication'
 => 'Veröffentlicht',
'Scénaristes'
 => 'Serien',
'Dessinateurs'
 => 'Herausgeber',
'Coloristes'
 => 'Artisten',
'Réalisateurs'
 => 'Regisseur',
'Acteurs'
 => 'Schauspieler',
'Pays'
 => 'Land',
'Années de sortie'
 => 'Veröffentlicht',
'Plateformes'
 => 'Plattformen',
'Développeurs'
 => 'Entwickler',
'Artistes'
 => 'Artisten',
'Origines'
 => 'Herkunft',
'Compositeurs'
 => 'Komponisten',
'Producteurs'
 => 'Produzenten',
'Labels'
 => 'Label',
'Cliquez dans le coin inférieur droit pour voir une autre image'
 => 'Klicken Sie in die rechte untere Ecke, um ein anderes Bild anzuzeigen.',
'Genre'
 => 'Genre',
'Langue'
 => 'Sprache',
'Sous-titre'
 => 'Untertitel',
'Saison'
 => 'Staffel',
'Épisode'
 => 'Folge',
'Source'
 => 'Quelle',
'Auteur(s)'
 => 'Autoren',
'Illustrateur(s)'
 => 'Illustrator (en)',
'Editeur'
 => 'Herausgeber',
'Joueurs'
 => 'Spieler',
'Durée'
 => 'Dauer',
'Mécanisme(s)'
 => 'Mechanismus (en)',
'Extension de'
 => 'Erweiterung von',
'Parcourir'
 => 'Durchsuchen',
'Couverture de %s'
 => 'Cover für %s',
'Autres images de %s'
 => 'Andere Bilder von %s',
'Suivant'
 => 'Nächste',
'Série'
 => 'Serie',
'Éditeur'
 => 'Herausgeber',
'ISBN'
 => 'ISBN',
'Pages'
 => 'Seiten',
'Publié le'
 => 'Herausgegeben am',
'Ajouté le'
 => 'Hinzugefügt am',
'Commentaires'
 => 'Kommentare',
'Scénario'
 => 'Serien',
'Dessin'
 => 'Zeichnung',
'Couleurs'
 => 'Spieler',
'Collection'
 => 'Sammlung',
'Source Web'
 => 'Quelle',
'Réalisateur'
 => 'Regisseur',
'Sortie'
 => 'Veröffentlichung',
'Plateforme'
 => 'Plattform',
'Développeur'
 => 'Entwickler',
'Date de sortie'
 => 'Erscheinungsdatum',
'Captures d\'écran'
 => 'Screenshots',
'Capture d\'écran'
 => 'Screenshot',
'Artiste'
 => 'Künstler',
'Label'
 => 'Label',
'Compositeur'
 => 'Komponist',
'Producteur'
 => 'Produzent',
'Format'
 => 'Format',
'Origine'
 => 'Herkunft',
'Emplacement'
 => 'Ort',
'Tags'
 => 'Tags',
'Lien web'
 => 'Weblink',
'Plus d\'info'
 => 'Weitere Informationen',
'Publié en'
 => 'Herausgegeben am',
'Détail'
 => 'Detail',
'URL'
 => 'URL',
'Veuillez patienter le chargement de cette page peut être long'
 => 'Bitte warten, das Laden dieser Seite kann länger dauern',
'masquer ce message'
 => 'Diesen Beitrag ausblenden',
'Navigation'
 => 'Navigation',
'Page d\'accueil'
 => 'Startseite',
'Toute la collection'
 => 'Komplette Sammlung',
'Classer par'
 => 'Sortieren nach',
'Diffusé'
 => 'Ausgestrahlt',
'Chercher'
 => 'Suchen',
'dans'
 => 'in',
'Titre'
 => 'Titel',
'Recherche avancée'
 => 'Erweiterte Suche',
'S\'abonner'
 => 'Abonnieren',
'S\'abonner au flux RSS'
 => 'RSS-Feed abonnieren',
'Éléments affichés'
 => 'Angezeigt Objekte',
'aucun élément affiché'
 => 'kein Objekt angezeigt',
'un élément affiché'
 => 'ein Objekt angezeigt',
'%d éléments affichés'
 => '%d Objekte angezeigt',
'aucun élément filtré'
 => 'kein gefiltertes Objekt',
'un élément filtré'
 => 'ein gefiltertes Objekt',
'%d éléments filtrés'
 => '%d gefilterte Objekte',
'Collection mise à jour<br />le %s</li>'
 => 'Sammlung aktualisiert<br />am %s</li>',
'%e/%m/%y'
 => '%e/%m/%y',
'Les séries'
 => 'Serien',
'Les auteurs'
 => 'Autoren',
'Les éditeurs'
 => 'Herausgeber',
'Les genres'
 => 'Genres',
'Auteur'
 => 'Schriftsteller',
'Notation'
 => 'Notation',
'Date de publication'
 => 'Herausgegeban am',
'Date d\'ajout'
 => 'Hinzugefügt am',
'tous'
 => 'alle',
'titre'
 => 'Titel',
'série'
 => 'Serie',
'auteur'
 => 'Autor',
'éditeur'
 => 'Herausgeber',
'genre'
 => 'Genre',
'Les scénaristes'
 => 'Schriftsteller',
'Les dessinateurs'
 => 'Zeichner',
'Les coloristes'
 => 'Koloristen',
'Les collections'
 => 'Sammlungen',
'Nom'
 => 'Name',
'Scénariste'
 => 'Serien',
'Dessinateur'
 => 'Zeichner',
'Coloriste'
 => 'Kolorist',
'Les réalisateurs'
 => 'Regisseure',
'Les acteurs'
 => 'Schauspieler',
'réalisateur'
 => 'Regisseur',
'acteur'
 => 'Schauspieler',
'Editor'
 => 'Editor',
'Tous'
 => 'alle',
'plateforme'
 => 'Plattform',
'développeur'
 => 'Entwickler',
'artiste'
 => 'Künstler',
'label'
 => 'Label',
'compositeur'
 => 'Komponist',
'piste'
 => 'Titel',
'tags'
 => 'Tags',
'Année'
 => 'Jahr',
'Nationalité'
 => 'Produktionsland',
'Titres'
 => 'Titel',
'Tome'
 => 'Band',
'Synopsis'
 => 'Zusammenfassung',
'Pistes'
 => 'Titel',
'Publier en'
 => 'Erschienen am',
'Titre original'
 => 'Original Titel',
'Multicritères'
 => 'Mehrfachkriterien',
'Les critères vides seront ignorés.'
 => 'Leere Felder werden ignoriert.',
'Les dates peuvent être entrée sous la forme %s, 2007 (considéré comme 1er janvier 2007), ...'
 => 'Termine können im Format %s, 2007 (Entspricht dem 1. Januar 2007) eingegeben werden, ...',
'Dans les listes à choix multiples pour séléctionner plusieurs éléments maintenez pressé la touche contrôle.'
 => 'Für eine Mehrfachauswahl der Optionen in einer Liste, halten Sie die STRG-Taste gedrückt.',
'Requête'
 => 'Abfrage',
'Il est possible de réaliser une requête composée de plusieurs conditions afin d\'effectuer une recherche encore plus précise'
 => 'Es ist möglich, eine Abfrage aus mehreren Bedingungen durchzuführen, um eine noch genauere Suche durchzuführen.',
'Chercher dans'
 => 'Suche in',
'toute la base'
 => 'gesamten Daten',
'les résultats précédents'
 => 'die bisherigen Ergebnisse',
'ex:'
 => 'zB:',
'Champs contenant du texte'
 => 'Textfelder',
'contient'
 => 'enthält',
'ne contient pas'
 => 'enthält nicht',
'est (chaîne exacte)'
 => 'Ist (genaue Zeichenfolge)',
'n\'est pas (chaîne exacte)'
 => 'Ist nicht (genaue Zeichenfolge)',
'Champs numériques et dates'
 => 'Zahlenfelder und Termine',
'égal'
 => 'ist',
'différent'
 => 'unterschiedlich',
'plus petit'
 => 'kleiner',
'plus petit ou égal'
 => 'kleiner oder gleich',
'plus grand'
 => 'grösser',
'plus grand ou égal'
 => 'grösser oder gleich',
'Ajouter la condition'
 => 'Bedingung hinzufügen',
'Illustrateur'
 => 'Illustrator',
'Mécanisme'
 => 'Mechanismus',
'les resultats précédents'
 => 'die bisherigen Ergebnisse',
'Ajouté à la collection entre (dates)'
 => 'Zur Sammlung hinzugefügt zwischen (Datum)',
'Publié entre (dates)'
 => 'Veröffentlichung zwischen (Datum)',
'Nombre de pages entre'
 => 'Seitenanzahl zwischen',
'Note entre'
 => 'Bewertung zwischen',
'Informations générales'
 => 'Allgemeine Informationen',
'Note (0 à 10)'
 => 'Bewertung (0 bis 10)',
'Informations diverses'
 => 'Weitere Informationen',
'Nombre de pages'
 => 'Seitenanzahl',
'Édition'
 => 'Auflage',
'Traducteur'
 => 'Übersetzer',
'Format du livre'
 => 'Buchformat',
'Source des informations'
 => 'Quelle der Information',
'Commentaire'
 => 'Bemerkungen',
'Dates'
 => 'Dates',
'd\'ajout'
 => 'Erworben',
'de publication'
 => 'der Veröffentlichung',
'année de publication'
 => 'Erscheinungsjahr',
'Status/emprunts'
 => 'Status / Verliehen',
'Lu ?'
 => 'gelesen ?',
'Emprunté ?'
 => 'Verliehen ?',
'Emprunteur'
 => 'Entleiher',
'Date d\'emprunt'
 => 'verliehen am',
'Acteur'
 => 'Schauspieler',
'Rôle'
 => 'Rolle',
'Sorti entre (dates)'
 => 'Erschienen zwischen (Datum)',
'Titre Original'
 => 'Original Titel',
'Rôles'
 => 'Rollen',
'Nombre'
 => 'Nummer',
'Identifiant'
 => 'Identifier',
'Age minimum'
 => 'Altersfreigabe',
'Format Vidéo'
 => 'Videoformat',
'Région'
 => 'Region',
'Langues Audio'
 => 'Audio-Sprachen',
'Sous Titre'
 => 'Untertitel',
'de sortie'
 => 'Erscheinungsdatum',
'Vu ?'
 => 'gesehen ?',
'Note'
 => 'Bewertung',
'Nom de la piste'
 => 'Titelname',
'Format de l\'album'
 => 'Format des Albums',
'par'
 => 'von',
'joueur(s)'
 => 'Autoren',
));
?>
