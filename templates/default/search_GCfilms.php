<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


/*
 * Partie centale de la page de recherche.
 * Variable disponible : $info, $collec, $item
 */
?>

<div id="content">

    <?php include TEMPLATE_MODEL_PATH_GCWEB.'/menu_'.$collec['type'].'.php' ?>

    <div id="search">

        <div class="element">
            <div class="box">

                <h2><?php echo __('Recherche avancée') ?></h2>

                <fieldset><legend><?php echo __('Multicritères') ?></legend>

                <?php aff_search('start') ?>

                <ul>
                    <li><?php echo __('Les critères vides seront ignorés.') ?></li>
                    <li><?php printf(__('Les dates peuvent être entrée sous la forme %s, 2007 (considéré comme 1er janvier 2007), ...'),date(strtolower($conf['fomatDate']))) ?></li>
                    <li><?php echo __('Dans les listes à choix multiples pour séléctionner plusieurs éléments maintenez pressé la touche contrôle.') ?></li>
                </ul>

                <div class="searchgroup searchstring">
                    <fieldset><legend><?php echo __('Titre') ?></legend>
                        <p><?php aff_search('title','str') ?></p>
                    </fieldset>

                    <fieldset><legend><?php echo __('Synopsis') ?></legend>
                        <p><?php aff_search('synopsis','str') ?></p>
                    </fieldset>
                </div>

                <div class="searchgroup searchlistmultiple">
                    <fieldset><legend><?php echo __('Réalisateur') ?></legend>
                        <p><?php aff_search('director','listmultiple') ?></p>
                    </fieldset>

                    <fieldset><legend><?php echo __('Acteur') ?></legend>
                        <p><?php aff_search('actors_without_roles','listmultiple') ?></p>
                    </fieldset>

                    <fieldset><legend><?php echo __('Rôle') ?></legend>
                        <p><?php aff_search('roles','listmultiple') ?></p>
                    </fieldset>

                    <fieldset><legend><?php echo __('Pays') ?></legend>
                        <p><?php aff_search('country','listmultiple') ?></p>
                    </fieldset>

                    <fieldset><legend><?php echo __('Genre') ?></legend>
                        <p><?php aff_search('genre','listmultiple') ?></p>
                    </fieldset>
                </div>

                <div class="searchgroup searchbetween">
                    <fieldset><legend><?php echo __('Ajouté à la collection entre (dates)') ?></legend>
                        <p><?php aff_search('added','>='); aff_search('&amp;added','<=') ?></p>
                    </fieldset>

                    <fieldset><legend><?php echo __('Sorti entre (dates)') ?></legend>
                        <p><?php aff_search('date','>='); aff_search('&amp;date','<=') ?></p>
                    </fieldset>

                    <fieldset><legend><?php echo __('Note entre') ?></legend>
                        <p><?php aff_search('rating','>=','',array('0*',1,2,3,4,5,6,7,8,9,10));  aff_search('&amp;rating','<=','',array(0,1,2,3,4,5,6,7,8,9,'10*')) ?></p>
                    </fieldset>
                </div>

                <div class="searchgroup searchsubmit">
                    <p><?php aff_search('submit') ?></p>
                </div>

                <?php aff_search('end') ?>
                </fieldset>

                <fieldset><legend><?php echo __('Requête') ?> :</legend>
                <p><?php echo __('Il est possible de réaliser une requête composée de plusieurs conditions afin d\'effectuer une recherche encore plus précise') ?>.</p>
                <p>
                    <?php echo __('Chercher dans') ?>
                    <select id="js_and_or">
                        <option value="|"><?php echo __('toute la base') ?></option>
                        <option value="&amp;"><?php echo __('les resultats précédents') ?></option>
                    </select>
                    :
                 <select id="js_champ">
                      <optgroup label="<?php echo __('Informations générales') ?>">
                          <option value="title"><?php echo __('Titre') ?></option>
                          <option value="original"><?php echo __('Titre Original') ?></option>
                          <option value="director"><?php echo __('Réalisateur') ?></option>
                          <option value="actors_without_roles"><?php echo __('Acteur') ?></option>
                          <option value="roles"><?php echo __('Rôles') ?></option>
                          <option value="genre"><?php echo __('Genre') ?></option>
                          <option value="country"><?php echo __('Nationalité') ?></option>
                          <option value="time"><?php echo __('Durée') ?></option>
                          <option value="synopsis"><?php echo __('Synopsis') ?></option>
                          <option value="rating"><?php echo __('Note (0 à 10)') ?></option>
                      </optgroup>
                      <optgroup label="<?php echo __('Informations diverses') ?>">
                          <option value="number"><?php echo __('Nombre') ?></option>
                          <option value="format"><?php echo __('Format') ?></option>
                          <option value="identifier"><?php echo __('Identifiant') ?></option>
                          <option value="serie"><?php echo __('Série') ?></option>
                          <option value="rank"><?php echo __('Épisode') ?></option>
                          <option value="age"><?php echo __('Age minimum') ?></option>
                          <option value="video"><?php echo __('Format Vidéo') ?></option>
                          <option value="region"><?php echo __('Région') ?></option>
                          <option value="audio"><?php echo __('Langues Audio') ?></option>
                          <option value="subt"><?php echo __('Sous Titre') ?></option>
                          <option value="place"><?php echo __('Emplacement') ?></option>
                          <option value="comment"><?php echo __('Commentaire') ?></option>
                      </optgroup>
                      <optgroup label="<?php echo __('Dates') ?>">
                          <option value="added"><?php echo __('d\'ajout') ?></option>
                          <option value="date"><?php echo __('de sortie') ?></option>
                      </optgroup>
                      <optgroup label="<?php echo __('Status/emprunts') ?>">
                          <option value="seen"><?php echo __('Vu ?') ?></option>
                          <option value="borrowings"><?php echo __('Emprunté ?') ?></option>
                          <option value="borrower"><?php echo __('Emprunteur') ?></option>
                          <option value="lentDate"><?php echo __('Date d\'emprunt') ?></option>
                      </optgroup>
                  </select>
                  <select id="js_cond">
                        <optgroup label="<?php echo __('Champs contenant du texte') ?>">
                            <option value="="><?php echo __('contient') ?></option>
                            <option value="!="><?php echo __('ne contient pas') ?></option>
                            <option value="=="><?php echo __('est (chaîne exacte)') ?></option>
                            <option value="!=="><?php echo __('n\'est pas (chaîne exacte)') ?></option>
                        </optgroup>
                        <optgroup label="<?php echo __('Champs numériques et dates') ?>">
                            <option value="==">= <?php echo __('égal') ?></option>
                            <option value="!==">&ne; <?php echo __('différent') ?></option>
                            <option value="&lt;">&lt; <?php echo __('plus petit') ?></option>
                            <option value="&lt;=">&le; <?php echo __('plus petit ou égal') ?></option>
                            <option value="&gt;">&gt; <?php echo __('plus grand') ?></option>
                            <option value="&gt;=">&ge; <?php echo __('plus grand ou égal') ?></option>
                        </optgroup>
                    </select>
                    <input id="js_value" type="text" />
                    <input type="button" value="<?php echo __('Ajouter la condition') ?>" onclick="search_addcond('request')" />
                </p>
                <?php aff_search('start') ?>
                <p><?php aff_search('request','','rows="60" cols="20" id="request"') ?></p>
                <p><?php aff_search('submit') ?></p>
                <?php aff_search('end') ?>
                </fieldset>
            </div>
        </div>
    </div>
</div>

