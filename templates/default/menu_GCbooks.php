<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
?>
    <div id="menu">

        <div id="infoCollec" class="box">
            <h2><?php aff($collec['title']) ?></h2>
            <p class="description"><?php aff($collec['description']) ?></p>
        </div>

        <div id="navigation" class="box">
            <h2><?php echo __('Navigation') ?></h2>
            <ul>
                <li><a href="<?php echo URLRACINE_GCWEB ?>"><?php echo __('Page d\'accueil') ?></a></li>
                <li><a href="<?php aff_hrefModel($collec['parentmodel']) ?>"><?php echo __('Toute la collection') ?></a></li>
            </ul>
            <ul>
                <li><a href="<?php aff_hrefModel('cloud') ?>#series"><?php echo __('Les séries') ?></a></li>
                <li><a href="<?php aff_hrefModel('cloud') ?>#auteurs"><?php echo __('Les auteurs') ?></a></li>
                <li><a href="<?php aff_hrefModel('cloud') ?>#editeurs"><?php echo __('Les éditeurs') ?></a></li>
                <li><a href="<?php aff_hrefModel('cloud') ?>#genre"><?php echo __('Les genres') ?></a></li>
                <li><a href="<?php aff_hrefModel('cloud') ?>#annees"><?php echo __('Années de publication') ?></a></li>
            </ul>
        </div>

        <div id="classer" class="box">
        <?php if (($collec['model'] == 'item' )  ||
                  ($collec['model'] == 'cloud' ) ||
                  ($collec['model'] == 'search' )) { ?>
            <?php echo __('cette vue n\'est pas classable') ?>
        <?php } else { ?>
            <h2><?php echo __('Classer par') ?></h2>
            <ul>
                <li><?php echo __('Titre') ?>
                    <a <?php if (isSortKey('titleASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('titleASC') ?>">↓</a><a <?php if (isSortKey('titleDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('titleDSC') ?>">↑</a>
                </li>
                <li><?php echo __('Série') ?>
                    <a <?php if (isSortKey('serieASC,rankASC,titleASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('serieASC,rankASC,titleASC') ?>">↓</a><a <?php if (isSortKey('serieDSC,rankDSC,titleDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('serieDSC,rankDSC,titleDSC') ?>">↑</a>
                </li>
                <li><?php echo __('Auteur') ?>
                    <a <?php if (isSortKey('authorsASC,titleASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('authorsASC,titleASC') ?>">↓</a><a <?php if (isSortKey('authorsDSC,titleDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('authorsDSC,titleDSC') ?>">↑</a>
                </li>
                <li><?php echo __('Éditeur') ?>
                    <a <?php if (isSortKey('publisherASC,titleASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('publisherASC,titleASC') ?>">↓</a><a <?php if (isSortKey('publisherDSC,titleDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('publisherDSC,titleDSC') ?>">↑</a>
                </li>
                <li><?php echo __('Notation') ?>
                    <a <?php if (isSortKey('ratingDSC,titleASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('ratingDSC,titleASC') ?>">↓</a><a <?php if (isSortKey('ratingASC,titleDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('ratingASC,titleDSC') ?>">↑</a>
                </li>
                <li><?php echo __('Date de publication') ?>
                    <a <?php if (isSortKey('publicationDSC,titleASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('publicationDSC,titleASC') ?>">↓</a><a <?php if (isSortKey('publicationASC,titleDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('publicationASC,titleDSC') ?>">↑</a>
                </li>
                <li><?php echo __('Date d\'ajout') ?>
                    <a <?php if (isSortKey('addedDSC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('addedDSC') ?>">↓</a><a <?php if (isSortKey('addedASC')) echo 'class="activeSort" '; ?>href="<?php aff_hrefSortBy('addedASC') ?>">↑</a>
                </li>
            </ul>
        <?php } ?>
        </div>

        <div id="menusearch" class="box">
            <?php aff_search('start') ?>
            <h2><?php echo __('Chercher') ?></h2>
            <p><?php /*afficher la liste de champs de recherche*/ aff_search('keyword') ?></p>
            <p><?php echo __('dans') ?> :
            <?php /*afficher la liste de chmps de recherche*/
                aff_search('in_champs',array(
                        array('title=%s,|serie=%s,|authors=%s,|publisher=%s,|genre=%s',__('tous')),
                        array('title=%s',__('titre')),
                        array('serie=%s',__('série')),
                        array('authors=%s',__('auteur')),
                        array('publisher=%s',__('éditeur')),
                        array('genre=%s',__('genre'))
                    ))
            ?>
            </p>
            <p><?php aff_search('submit') ?></p>
            <?php aff_search('end') ?>
            <p><a href="<?php aff_hrefModel('search') ?>"><?php echo __('Recherche avancée') ?></a></p>
        </div>

        <div id="rss" class="box">
            <h2><?php echo __('S\'abonner') ?></h2>
            <ul class="rss">
                <li><a title="<?php echo __('S\'abonner au flux RSS') ?>" href="<?php aff_hrefModel('rss',False) ?>"><?php echo __('Toute la collection') ?></a></li>
                <li><a title="<?php echo __('S\'abonner au flux RSS') ?>" href="<?php aff_hrefModel('rss',True) ?>"><?php echo __('Éléments affichés') ?></a></li>
            </ul>
        </div>

        <div id="info" class="box">
            <h2><?php echo __('Informations') ?></h2>
            <ul>
                <li>
                <?php
                    if ($collec['nbItems'] == 0)    echo __('aucun élément affiché');
                    elseif ($collec['nbItems'] == 1)echo __('un élément affiché');
                    else                            printf(__('%d éléments affichés'),$collec['nbItems']);
                    echo '<br />';
                    $nbfiltre = $collec['nbItemsBDD'] - $collec['nbItems'];
                    if ($nbfiltre == 0)             echo __('aucun élément filtré');
                    elseif ($nbfiltre == 1)         echo __('un élément filtré');
                    else                            printf (__('%d éléments filtrés'),$nbfiltre);
                ?>
                </li>

                <li><?php printf (__('Collection mise à jour<br />le %s</li>'),strftime(__('%e/%m/%y'),filemtime(DIR_GCWEB.'collections/'.$collec['dir'].$collec['xml']))) ?>
            </ul>
        </div>

    </div>
    <!-- Fin du menu -->
