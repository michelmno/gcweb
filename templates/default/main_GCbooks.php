<?php
/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


/*
 * Cette partie s'intègre dans la boucle des collections de main.php.
 * Variable disponible : idem main.php
 */


?>
                    <div id="id_<?php aff($collec['id']) ?>_<?php aff($lastItem['id'])?>" class="element" onmouseover="javascript:changeInfo('info_<?php aff($collec['id']) ?>_<?php aff($lastItem['id'])?>')" onmouseout="javascript:hide('info_<?php aff($collec['id']) ?>_<?php aff($lastItem['id'])?>')">
                        <div class='legend' id="info_<?php aff($collec['id']) ?>_<?php aff($lastItem['id']) ?>" style="margin-top: 160px">
                            <h3><a href="<?php aff_hrefitem($lastItem)?>" ><?php aff($lastItem['title']) ?></a></h3>
                            <ul>
                                <?php if (test($lastItem['serie']))     {?> <li><span class="label"><?php echo __('Série') ?> :</span>      <span class="info"><?php aff_filter('serie==',$lastItem['serie']) ?>
                                                                                                                                                              (<?php aff($lastItem['rank'])                   ?>)</span></li><?php } ?>
                                <?php if (test($lastItem['authors']))   {?> <li><span class="label"><?php echo __('Auteurs') ?> :</span>    <span class="info"><?php aff_filter('authors==',$lastItem['authors']) ?></span></li><?php } ?>
                                <?php if (test($lastItem['publisher'])) {?> <li><span class="label"><?php echo __('Éditeur') ?> :</span>    <span class="info"><?php aff_filter('publisher==',$lastItem['publisher']) ?> </span></li><?php } ?>
                                <?php if (test($lastItem['genre']))     {?> <li><span class="label"><?php echo __('Genre') ?> :</span>      <span class="info"><?php aff_filter('genre==',$lastItem['genre']) ?></span></li><?php } ?>
                                <?php if (test($lastItem['year']))      {?><li><span class="label"><?php echo __('Publié en') ?> :</span>   <span class="info"><?php aff_filter('year==',$lastItem['year']) ?> </span></li><?php } ?>
                                <?php if (test($lastItem['web']))       {?> <li><a href="<?php aff($lastItem['web'])?>"><?php echo __('Source') ?></a></li><?php } ?>
                            </ul>
                            <?php echo join("\n", $lastItem['array_add_to_all_pages']); ?>
                        </div>

                        <a href="<?php aff_hrefItem($lastItem['id'])?>" title="<?php aff($lastItem['title'])?>">
                             <img class="image" src="<?php aff_image($lastItem['cover'],'auto',160) ?>" <?php aff_attrsize_image($lastItem['cover'],'auto',160) ?> alt="<?php printf(__('Couverture de %s'),convert($lastItem['title'])) ?>" />
                        </a>
                    </div>
