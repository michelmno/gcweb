/*
 *      This file is a part of GCweb (unoffical web render for GCstar)
 *      Copyright (c) 2007 Jonas Fourquier <http://jonas.tuxfamily.org> and contributors
 *
 *      GCweb is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


/* Effet javascript simplifier pour les navigateurs au fonction javascript pas au top */

/*** Intialisation variable ***/
var oldmenu = '';
var blockmenu = '';


/*** Fonctions ***/


function show(id) {
    document.getElementById(id).style.visibility = "visible";
}


function hide(id) {
    var hide_id = document.getElementById(id);
    if (hide_id) {
        hide_id.style.visibility = "hidden";
    }
}


function showMenu(menu) {
    if (oldmenu !== '') {
        hide(oldmenu);
        oldmenu = '';
    } else {
        show(menu);
        oldmenu = menu;
    }
}


function changeMenu(menu) {
    if (oldmenu !== '' | oldmenu == menu) {
        hide(oldmenu);
        show(menu);
        oldmenu = menu;
    }
}


function hideoldMenu() {
    if (oldmenu !== '') {
        hide(oldmenu);
        oldmenu = '';
    }
}


function blockVisibilityMenu() {
    blockmenu = oldmenu;
    oldmenu = '';
}


function unblockVisibilityMenu() {
    oldmenu = blockmenu;
}

var oldinfo = '';
function changeInfo(info) {
    if (oldinfo !== '') {
        hide(oldinfo);
    }
    show(info);
    oldinfo = info;
}


function hidedelay(id) {
    setTimeout('document.getElementById("' + id + '").style.visibility="hidden"',  2000);
}


var noimg = 1;
function switchimg(nbimg) {
    var old_element = document.getElementById('img' + noimg);
    if (noimg == nbimg) {
        noimg = 1;
    } else {
        noimg ++;
    }
    var new_element = document.getElementById('img' + noimg);

    old_element.style.display = 'none';
    new_element.style.display = 'block';
}


function load_img(url, parent_id) {
    /*charge l'image d'un id*/
    var imgNode = document.createElement('img');
    imgNode.setAttribute('src', url);
    document.getElementById(parent_id).appendChild(imgNode);
}


function search_addcond(out_id) {
    var and_or = document.getElementById('js_and_or').value;
    var champ = document.getElementById('js_champ').value;
    var cond = document.getElementById('js_cond').value;
    var value = document.getElementById('js_value').value;

    if (document.getElementById(out_id).value === '') {
        document.getElementById(out_id).value = and_or + champ + cond + value;
    } else {
        document.getElementById(out_id).value += ',\n' + and_or + champ + cond + value;
    }
}
