<?php
/*** fichier lang généré par po2phparray.py by Jonas (http://jonas.tuxfamily.org/wiki/po2phparray) ***/

$lang = array_merge($lang, array(
'Utilisateur ou mot de passe incorrect'
 => 'Benutzer oder Passwort falsch',
'Le nom d\'utilisateur et le mot de passe sont obligatoires'
 => 'Benutzername und Kennwort erforderlich',
'Le fichier <code>/conf/config.php</code> et/ou son
            dossier <code>/conf</code> n\'est pas accessible en écriture. Changez les droits
            ou créez manuellement ce fichier en vous basant sur <code>/conf/config.example.php</code>'
 => 'Die Datei <code>/conf/config.php</code> und / oder
            der Ordner <code>/conf</code> ist schreibgeschützt.
             Ändern sie die Zugriffsberechtigungen
            oder erstellen sie diese Datei anhand des Musters <code>/conf/config.example.php</code>',
'Erreur dans le fichier de configuration'
 => 'Fehler in der Konfigurationsdatei',
'Enregistrement impossible'
 => 'Speichern nicht möglich',
'Le fichier de configuration a été enregistré'
 => 'Konfigurationsdatei gespeichert',
'Le fichier de configuration n\'a pas été enregistré'
 => 'Konfigurationsdatei nicht gespeichert',
'n\'est pas une chaîne'
 => 'ist keine Kette',
'n\'est pas un chiffre'
 => 'ist keine Zahl',
'n\'est pas un vrai/faux'
 => 'ist kein True / False',
'n\'est pas une liste au correctement formatée'
 => 'ist keine korrekt formatierte Liste',
'n\'est pas d\'un type connu'
 => 'ist von einem nicht bekannten Typ',
'Fichiers de mise en cache des pages suprimées'
 => 'Seiten Cache gelöscht',
'Fichiers de mise en cache des images suprimées'
 => 'Bilder Cache gelöscht',
'Le dossier racine n\'est pas inscriptible le renomage de
                    <code>config.php</code> ne peux être effectué. Renommez ce fichier à l\'aide de
                    votre client ftp.'
 => 'Der Stammordner ist nicht beschreibbar.
                    Da die Umbenennung von <code>config.php</ code> nicht ausgeführt werden kann,
                    benennen Sie diese Datei bitte mit Ihrem FTP-Client um.',
'Attention : nom de page invalide, renommage abandonné'
 => 'Warnung: ungültiger oder leerer Seitenname.',
'Configuration de GCWeb'
 => 'Konfiguration von GCweb',
'Vos collections'
 => 'Ihre Sammlungen',
'wiki de GCweb'
 => 'Wiki von GCweb',
'thèmes pour GCweb'
 => 'Designs für GCweb',
'plugins pour GCweb'
 => 'Plugins für GCweb',
'actualités de GCweb'
 => 'Neuigkeiten von GCweb',
'Bienvenue dans la page de configuration de GCweb.'
 => 'Willkommen auf der GCweb-Konfigurationsseite.',
'<h3>Avertissement</h3>

                <p>GCstar récupère des informations sur des sites internet divers, ce qui pour une
                utilisation privée ne pose généralement pas de problème. Cependant, en affichant
                ces informations sur votre site web, GCweb rend publiques ces informations.</p>

                <p>La personne installant GCweb doit vérifier que les informations mises en ligne
                ne sont pas protégées par des règles sur la propriété intellectuelle interdisant
                ce type d\'utilisation.</p>

                <p>Note : avec les thèmes officiels les sources sont citées par l\'intermédiaire du lien "sources".
                Cependant, avec des thèmes alternatifs, cette information peut être masquée.</p>'
 => '<h3>Warnung</h3>

                <p>GCstar ruft Informationen von verschiedenen Websites ab, die für den privaten Gebrauch
                in der Regel kein Problem sind. Allerdings macht GCweb diese Informationen durch
                die Anzeige auf Ihrer Website öffentlich.</p>

                <p>Die Person die GCweb installiert muss sicherstellen, dass die Informationen Online nicht
                durch Regeln des geistigen Eigentums geschützt sind, die eine solche Verwendung verbieten.</p>

                <p>Hinweis: Mit den offiziellen Design werden die Quellen über den Link "Quellen" angegeben.
                Mit alternativen Design können diese Informationen jedoch verborgen werden.</p>',
'En cochant cette case vous attestez avoir pris connaissance de ces informations'
 => 'Wenn Sie den Haken setzen, bestätigen Sie, dass Sie diese Information gelesen haben',
'Prérequis'
 => 'Voraussetzungen',
'Version de PHP (5 ou plus)'
 => 'PHP Version (5 oder höher)',
'votre version est'
 => 'Ihre Version ist',
'Consultez
                    la documentation de votre hébergeur, vous pouvez probablement
                    activer PHP5'
 => 'Überprüfen
                    Sie die Dokumentation Ihres Hosters. Sie können PHP5
                    wahrscheinlich aktivieren',
'Les "magic_quotes_runtime()" semble être activé et leur désactivation
                    est impossible. Le générateur de fichier de configuration (cette page) et le générateur
                    de thème ne fonctionneront pas mais les autres fonctionnalités (utilisation normale) ne
                    poseront pas de problème. Pour créer vos fichier de configuration consultez
                    <a href="./?redirect=http%3A%2F%2Fjonas.tuxfamily.org%2Fwiki%2Fgcweb%2Fdocumentation%2Ffichier_de_configuration">
                        la documentation
                    </a>.'
 => 'Die "magic_quotes_runtime()" wird aktiviert und kann nicht
                    deaktiviert werden. Der Konfigurationsdatei-Generator (diese Seite) und
                    der Design-Generator funktionieren nicht, aber die anderen Features (normaler Gebrauch)
                    sind kein Problem. Um Ihre Konfigurationsdateien zu erstellen, lesen Sie
                    <a href="./?redirect=http%3A%2F%2Fjonas.tuxfamily.org%2Fwiki%2Fgcweb%2Fdocumentation%2Ffichier_de_configuration">
                        die Dokumentation
                    </a>.',
'Les bibliotèques GD sont présentes et permettront de redimensionner les images de type'
 => 'Die GD-Bibliotheken sind vorhanden und erlauben die Größenänderung der Bilder vom Typ',
'Les bibliotèques GD sont absentes, GCweb ne pourra pas fonctionner'
 => 'GD-Bibliotheken fehlen, GCweb funktioniert nicht',
'Le dossier "<code>/conf</code>" n\'est pas inscriptibles. Le fichier de
                    configuration ne pourra pas être enregistré'
 => 'Der Ordner "<code> / conf </ code> " ist nicht beschreibbar. Die Konfigurationsdatei kann nicht gespeichert werden',
'L\'un ou plusieurs de ces fichiers ne sont pas inscriptible :'
 => 'Eine oder mehrere dieser Dateien sind nicht beschreibbar:',
'Le dossiers "<code>/cache</code>" et ses sous-dossiers doivent être inscriptible sinon GCweb
                    ne pourra pas fonctionner (sauf si vous désactivé le complêtement le cache mais ceci est déconseillé)'
 => 'Der Ordner "<code>/cache</code>" und seine Unterordner müssen beschreibbar sein, sonst wird
                    GCweb nicht funktionieren (es sei denn, Sie deaktivieren den Cache vollständig, das ist aber veraltet)',
'Aucun fichier de thème n\'a été trouvé'
 => 'Keine Design Datei gefunden',
'Aucun fichier de sauvegarde GCstar n\'a été trouvé.
                    Envoyez votre/vos fichier/s de sauvergarde dans le dossier
                    <code>/collections</code> à l\'aide de votre client ftp favori'
 => 'Es wurde keine GCstar-Sicherungsdatei gefunden.
                    Kopieren Sie die Sicherungsdatei Ihrer Sammlung in den Ordner
                    <code>/collections</code> Nutzen Sie hierfür Ihren Lieblings-FTP-Client',
'Aucun dossier d\'images n\'a été trouvé, envoyez votre/vos
                    <strong>dossier/s</strong> contenant les images de votre/vos collection/s
                    dans le dossier <code>/collections</code> à l\'aide de votre client ftp
                    favori'
 => 'Es wurden keine Bilddateien gefunden, kopieren Sie Ihren
                    <strong>Ordner</strong> mit den Bildern aus Ihrer Sammlung in den
                    Ordner <code>/collections</code> Nutzen Sie hierfür Ihren Lieblings-FTP-Client',
'Attention : Le model de collection %s est inconnu de GCweb. Veuillez copier <a href="./?redirect=http%%3A%%2F%%2Fwiki.gcstar.org%%2Ffr%%2Fuser_models"> modèle de collection GCstar</a> dans le dossier <code>/conf/GCmodels/</code> de GCweb et réenregistrer cette page.'
 => 'Warnung: Der Typ der Sammlung %s ist GCweb nicht bekannt. Bitte kopieren Sie <a href="./?redirect=http%%3A%%2F%%2Fwiki.gcstar.org%%2Ffr%%2Fuser_models">die GCstar Sammlungsvorlage von GCstar</a> in den GCweb-Ordner <code>/conf/GCmodels/</ code> und speichern diese Seite erneut.',
'Revérifier'
 => 'Überprüfen',
'Fichier de configuration consultable'
 => 'durchsuchbare Konfigurationsdatei',
'<p>Un fichier <code>config.php</code> se trouve à la racine de
                l\'installation de GCweb. Afin d\'empêcher la consultation de cette page et
                d\'augmenter la sécurité de GCweb, il est conseillé de renommer ce fichier.
                Cette page sera ensuite consultable via l\'url
                <code>http://votre_site.tdl/gcweb/nom_du_fichier.php</code>.<br />
                (Notez que pour encore plus de sécurité vous pouvez supprimer ce fichier
                une fois la configuration terminée).</p>'
 => '<p>Die Datei <code>config.php</ code> befindet sich im Stammverzeichnis
                der GCweb-Installation. Um zu verhindern, dass diese Konfigurationsseite
                leicht erreicht und die Sicherheit von GCweb erhöht wird, empfiehlt es sich,
                diese Datei umzubenennen. Diese Seite steht dann über folgende URL zur Verfügung
                <code>http://Seitenname.tdl/gcweb/Dateiname.php</code>.<br />
                (Hinweis: Für noch mehr Sicherheit können Sie nach
                der konfiguration diese Datei löschen).</p>',
'Nouveau nom'
 => 'Neuer Name',
'Entrez le nom la page <strong>sans</strong> l\'extention <code>.php</code> et
                    <strong>sans</strong> caractères spéciaux'
 => 'Geben Sie den Namen der Seite <strong>ohne</ strong> die Erweiterung <code>.php</ code> und
                    <strong>ohne</ strong> ein Sonderzeichen ein.',
'Général'
 => 'Global',
'Titre du site'
 => 'Titel der Seite',
'Description'
 => 'Beschreibung',
'Code html possible'
 => 'Html Code möglich',
'Utilisateur'
 => 'Benutzer',
'Mot de passe'
 => 'Passwort',
'laissez-le vide pour ne pas le changer'
 => 'Leer lassen um es nicht zu ändern',
'Langue de l\'interface'
 => 'Sprache des Interfaces',
'Thème'
 => 'Design',
'Visionner et télécharger des thèmes'
 => 'Designs anzeigen und herunterladen',
'Nombre d\'éléments par page'
 => 'Anzahl der Objekte pro Seite',
'Champs vide'
 => 'Leeres Feld',
'Quand un élément n\'a pas été complété dans GCstar. GCweb le remplacera
                    par cette chaîne'
 => 'Falls ein Element in GCstar nicht abgeschlossen wurde, wird GCweb dies mit
                    diesem String ersetzen',
'Non à MSIE'
 => 'Nein zum MSIE',
'Affiche un message sur internet explorer précisant que
                    ce navigateur respecte très mal les standards du web et qu\'il est possible que les
                    pages du site s\'affichent mal. Vous pouvez remplacer le message par défaut en
                    rédigeant votre propre message ci-dessous'
 => 'Zeigt eine Nachricht im Internet Explorer an, in der angegeben wird,
                    dass dieser Browser die Internet Standards nicht richtig unterstützt und dass es möglich ist,
                    dass Teile der Website fehler aufweisen. Sie können die Standardnachricht ersetzen,
                    indem Sie Ihre eigene Nachricht unten eintippen',
'Texte personnalisé NonMSIE'
 => 'Personaliserter NonMSIE Text',
'Générateur'
 => 'Generator',
'Permet de créer les pages de thème pour un type de collection non
                    supportée ou d\'en remplacer une qui ne vous plait pas
                    (<a href="./?redirect=http%3A%2F%2Fjonas.tuxfamily.org%2Fwiki%2Fgcweb%2Fconfiguration%23Ordre_de_tri+par_defaut">plus d\'information</a>).
                    Activez cette fonctionnalité uniquement quand vous en avez
                    l\'utilité'
 => 'Ermöglicht es Ihnen, Designseiten für einen nicht unterstützten
                    Auflistungstyp zu erstellen oder einen zu ersetzen, den Sie nicht mögen
                    (<a href="./?redirect=http%3A%2F%2Fjonas.tuxfamily.org%2Fwiki%2Fgcweb%2Fconfiguration%23Ordre_de_tri+par_defaut">mehr Informationen</a>).
                    Aktiviere diese Funktion nur, wenn du es brauchst',
'Cette collection serra supprimée lors de l\'enregistrement'
 => 'Diese Sammlung wird nach dem Speichern gelöscht',
'Annuler la suppression'
 => 'Löschen abbrechen',
'Supprimer cette collection'
 => 'Diese Sammlung löschen',
'La collection ne serra plus affichée par GCweb mais
                        les fichiers composant celle-ci ne seront pas supprimés'
 => 'Die Sammlung wird nicht mehr von GCweb angezeigt,
                        aber der Datenbestand wird nicht gelöscht.',
'Collection'
 => 'Sammlung',
'Titre de la collection'
 => 'Titel der Sammlung',
'Nom du fichier de sauvegarde'
 => 'Dateiname der Sammlung',
'Collection privée'
 => 'Private Sammlung',
'Si cochée, la collection sera cachée mais toujours accessible via'
 => 'Wenn diese Option gesetzt ist, wird die Sammlung versteckt, ist aber immer noch zugänglich über',
'son url direct'
 => 'ihre direkte URL',
'Attention, les visiteurs peuvent deviner très facilement cette url !'
 => 'Achtung: die Seitenbesucher können diese URL leicht erraten!',
'Dossier contenant les images'
 => 'Ordner mit Bilder',
'Ordre de tri par défaut'
 => 'Standard Sortierung',
'idASC fonctionnera avec tous les types de collection et correspond
                        à l\'ordre dans lequel vous avez ajouté les éléments. Utilisez
                        idDSC pour les avoir dans l\'ordre inverse. Il est égallement possible de les trier
                        selon <a href="./?redirect=http%3A%2F%2Fjonas.tuxfamily.org%2Fwiki%2Fgcweb%2Fdocumentation%2Fgenerateur_de_theme">
                        un ou plusieurs autres champs</a>'
 => 'idASC arbeitet mit allen Auflistungstypen und entspricht der Reihenfolge,
                        in der Sie die Artikel hinzugefügt haben. Verwenden Sie
                        idDSC, um sie in umgekehrter Reihenfolge zu haben. Es ist auch möglich, sie
                        nach <a href="./?redirect=http%3A%2F%2Fjonas.tuxfamily.org%2Fwiki%2Fgcweb%2Fdocumentation%2Fgenerateur_de_theme">
                        einem oder mehreren anderen Feldern</a> zu sortieren',
'Ajouter une collection'
 => 'Sammlung hinzufügen',
'Avancée'
 => 'Erweitert',
'Paramètres avancés de GCweb. Si vous ne savez pas ce que vous faites n\'y touchez pas !'
 => 'Erweiterte GCweb-Einstellungen. Wenn Sie nicht wissen, was Sie tun, lassen Sie die finger davon!',
'Paramètres de la mise en cache de la base de donnée'
 => 'Einstellungen zum Datenbankcache',
'Le traitement (filtrage et organisation) du fichier d\'enregistrement
                est une opération lourde. Afin d\'éviter que GCweb  nerefasse ce travail quand l\'utilisateur
                passe à la page suivante, la recherche est mise en cache'
 => 'Die Verarbeitung (Filterung und Organisation) der Registrierungsdatei
                ist eine umständliche Operation. Damit GCweb diesen Job nicht ausführt, wenn der Benutzer
                zur nächsten Seite wechselt, wird die Suche zwischengespeichert',
'Purger automatique tous les'
 => 'Automatisches löschen alle',
'minutes les fichiers de cache de la base de donnée qui non pas servis depuis le
                    nombre de minutes spécifié ci-dessous'
 => 'Minuten von Datenbank-Cachedateien, die nicht für die angegebene
                    Anzahl von Minuten verwendet wurden.',
'Les fichiers non utilisés depuis'
 => 'Dateien, die nicht benutzt werden seit',
'minutes'
 => 'Minuten',
'Ne pas utiliser de cache'
 => 'Keinen Cache verwenden',
'Cocher cette case est déconseillé (économise l\'espace disque
                    mais consomme plus de ressources et affiche moins rapidement les pages)'
 => 'Das setzen dieses Hakens ist veraltet (spart Speicherplatz,
                    verbraucht aber mehr Ressourcen und zeigt Seiten weniger schnell an)',
'Paramètres des images'
 => 'Bildeinstellungen',
'Qualitée des jpeg'
 => 'Qualität der jpeg Bilder',
'De 0 - qualité médiocre et poids très faible - à 100 - excellente
                    qualitée, images lourdes'
 => 'Von 0 - schlechte Qualität und sehr geringe Größe
                    bis 100 - ausgezeichnet Qualität und große Bilder.',
'Ne pas mettre en cache'
 => 'Nicht zwischenspeichern',
'Si activé, les images ne seront pas mises en cache. Il est fortement
                    déconseillé de cocher cette case. Vous économiserez de l\'espace-disque, mais
                    les ressources serveur seront très sollicitées et les images s\'afficheront très lentement,
                    voire partiellement'
 => 'Wenn diese Option aktiviert ist, werden die Bilder nicht zwischengespeichert.
                    Es wird dringend empfohlen, diesen Haken nicht zu setzen. Sie sparen Speicherplatz,
                    aber Serverressourcen werden stark beansprucht und Bilder werden
                    langsam oder nur teilweise angezeigt.',
'Purge automatique tous les'
 => 'Automatisch leeren nach',
'jours'
 => 'Tage',
'Les images non utilisées depuis'
 => 'Bilder, die seitdem nicht benutzt werden',
'Rééchantillonner les images'
 => 'Skalierung der Bilder',
'Si activé, crée des images de meilleure qualité.
                    Cependant cette fonction n\'est pas disponible chez tous
                    les hébergeurs. Désactivez cette option si la création des images ne
                    fonctionne pas ou est trop lente'
 => 'Wenn aktiviert, werden bessere Bilder erstellt.
                    Diese Funktion ist jedoch nicht bei allen Hosting-Anbietern verfügbar.
                    Deaktivieren Sie diese Option, wenn die Bilderstellung
                    nicht funktioniert oder zu langsam ist.',
'Paramètres divers'
 => 'Verschiedene Einstellungen',
'Chaîne à ignorer'
 => 'Zu ignorierende Strings',
'Liste des chaînes à ignorer lors du tri. ATTENTION
                    respectez rigoureusement la syntaxe (chaîne entre guillemet simple
                    séparée par une virgule et n\'oubliez pas de mettre l\'espace qui suit le déterminant
                    (n\'écrivez pas "<code>le</code>" mais écrivez "<code>le </code>",
                    <a href="./?redirect=http%3A%2F%2Fjonas.tuxfamily.org%2Fwiki%2Fgcweb%2Fdocumentation%2Fconfiguration_avancee%23chaines_a_ignoree">plus d\'information</a>).'
 => 'Liste der Zeichenfolgen, die beim Sortieren ignoriert werden. ACHTUNG
                    Halten Sie die Syntax ein (String in Hochkommas eingeschlossen, getrennt durch ein Komma und ein
                    Leerzeichen nach der Determinante, um diese nicht mit dem Anfang eines Wortes zu verwechseln
                    (Schreiben Sie nicht "<code>das</code>"  sondern schreiben Sie "<code>das </code>"), 
                    <a href="./?redirect=http%3A%2F%2Fjonas.tuxfamily.org%2Fwiki%2Fgcweb%2Fdocumentation%2Fconfiguration_avancee%23chaines_a_ignoree">mehr Informationen</a>).',
'Localisation'
 => 'Lokalisation',
'fr_FR.UTF8'
 => 'de_DE.utf8',
'Tapez le code de localisation (encodé en UTF-8) de votre pays. Ce code dépend du
                    système d\'exploitation sur lequel tourne le serveur de votre site web.
                    Vous pouvez laisser ce champs vide pour utiliser la paramêtre pas défaut
                    de votre serveur (<a href="./?redirect=http%3A%2F%2Ffr.php.net%2Fmanual%2Ffr%2Ffunction.setlocale.php">plus d\'information</a>).'
 => 'Geben Sie den Standortcode (in UTF-8 codiert) Ihres Landes ein. Dieser Code hängt von 
                    dem Betriebssystem auf dem Ihr Webserver läuft ab. 
                    Sie können dieses Feld leer lassen, um den Standardparameter  von Ihrem Server
                    zu verwenden (<a href="./?redirect=http%3A%2F%2Ffr.php.net%2Fmanual%2Ffr%2Ffunction.setlocale.php">mehr Informationen</a>).',
'Fuseau horaire'
 => 'Zeitzone',
'Europe/Paris'
 => 'Europe/Berlin',
'Selon la <a href="./?redirect=http%3A%2F%2Ffr.php.net%2Fmanual%2Ffr%2Ftimezones.php">
                    liste des fuseaux horaires</a> supportée par PHP.'
 => 'Abhängig von der <a href="./?redirect=http%3A%2F%2Ffr.php.net%2Fmanual%2Ffr%2Ftimezones.php">
                    Liste der Zeitzonen</a> unterstützt von PHP.',
'Format des dates GCstar'
 => 'Datumsformat von GCstar',
'D/M/Y'
 => 'D/M/Y',
'GCstar entregistre la date des éléments sous forme de chaînes (jj/mm/aa par exemple).
                    Sur cette base, le tri par ordre chronologique est impossible. GCweb peut essayer d\'interpréter
                    ces dates.'
 => 'GCstar verwendet für das Datum der Elemente folgende Formatierung (dd/mm/yy).
                    Bei dieser Formatierung ist die Sortierung in chronologischer Reihenfolge unmöglich.
                    GCweb kann versuchen das Dazum zu interpretieren.',
'Purge des fichiers de mise en cache'
 => 'Cache-Dateien löschen',
'Supprimera les fichiers de mise en cache lors de l\'enregistrement'
 => 'Löschen von Cached Files beim Speichern',
'De la base de donnée'
 => 'Aus der Datenbank',
'Des images'
 => 'von den Bilder',
'Enregistrement'
 => 'Anmeldung',
'Sauver les paramètres'
 => 'Einstellungen speichern',
));
?>
