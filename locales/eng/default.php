<?php
/*** fichier lang généré par po2phparray.py by Jonas (http://jonas.tuxfamily.org/wiki/po2phparray) ***/

$lang = array_merge($lang, array(
'La collection %d n\'existe pas'
 => 'The %d collection does not exist',
'erreur : impossible de charger le xml %s.'
 => 'Error : unable to load the %s xml',
'Attention ! Il est impossible de trier un tableau
                selon plus de 6 clefs. Le tableau a été trié selon les 6 premières clefs'
 => 'Warning ! unable to sort the table
               with more than 6 keys. The table has been sorted with the 6 first keys',
'Erreur de syntaxe sur la condition "%s"'
 => 'Syntax error on the "%s" condition',
'Le champs "%s" n\'existe pas, filtrage avec celui-ci impossible'
 => 'The "%s" field do not exist,  unable to filter with it',
'Aucun élément n\'a été trouvé.'
 => 'No entry has been found',
'jan'
 => 'jan',
'janvier'
 => 'january',
'fev'
 => 'feb',
'fevrier'
 => 'february',
'mar'
 => 'mar',
'mars'
 => 'march',
'avr'
 => 'apr',
'avril'
 => 'april',
'mai'
 => 'may',
'juin'
 => 'june',
'juil'
 => 'jul',
'juillet'
 => 'july',
'aout'
 => 'august',
'sept'
 => 'sep',
'septembre'
 => 'september',
'oct'
 => 'oct',
'octobre'
 => 'october',
'nov'
 => 'nov',
'novembre'
 => 'november',
'dec'
 => 'dec',
'decembre'
 => 'december',
));
?>
